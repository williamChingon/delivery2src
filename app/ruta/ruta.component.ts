import { Component, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { SweetAlertService } from '../services/sweetAlert.service';


declare let jQuery: any;

@Component({
  selector: 'ruta',
  templateUrl: './ruta.template.html'
})
export class Ruta implements  AfterViewInit{
	
	constructor (private sweetAlertService:SweetAlertService){}
	@ViewChild('modalCreate') public modalCreate: ModalDirective;
	@ViewChild('modalEdit') public modalEdit: ModalDirective;
	routeTypes: any = [

		{name:'tipo1',value:1},
		{name:'tipo2',value:2},
		{name:'tipo3',value:3},
		{name:'tipo4',value:4}
	];
	ngAfterViewInit() {
	    jQuery('.createModal').find('.caret').append('<i class="mdi mdi-chevron-down"></i>');
	}
	createRoute() {

		this.modalCreate.hide();
		this.launchAdvice('Creando',1,'');
	}
	editRoute() {

		this.modalEdit.hide();
		this.launchAdvice('Editando',3,'');
	}
	deleteRoute() {

		this.modalEdit.hide();
		this.launchConfirm('¿Desea usted continuar?',2,'',this.launchAdvice('hola',3,''));
	}
	launchAdvice(txt,type,accept) {

		 this.sweetAlertService.launchAdvice(txt, type,accept);
	}
	launchConfirm(txt,type,accept,cancel) {

		 this.sweetAlertService.launchConfirm(txt, type,accept,cancel);
	}
}
