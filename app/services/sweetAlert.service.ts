import { Injectable } from '@angular/core';
declare var swal: any;

@Injectable()
export class SweetAlertService {

  constructor() { }
  launchAdvice(txt, type, acceptClick): void {

  	 if(acceptClick == '' || acceptClick == null) {

        acceptClick = function() {

            console.log('nada que hacer');
        }
    }
    switch (type) {

        //success
        case 1:
           swal({
              title: "Muy bien!",
              text: txt,
              type: "success",
              showCancelButton: false,
              confirmButtonColor: "#B9DF90",
              confirmButtonText: "Aceptar",
              closeOnConfirm: true
            }).then(function () {
              
                
                    acceptClick();
                
              
            });
        break;
        //warning
        case 2:
            swal({
              title: "Atención",
              text: txt,
              type: "warning",
              showCancelButton: false,
              confirmButtonColor: "#B9DF90",
              confirmButtonText: "Aceptar",
              closeOnConfirm: true
            }).then(function () {
              
                
                    acceptClick();
                
              
            }); 
        break;
        //error
        case 3:
            swal({
              title: "Error",
              text: txt,
              type: "error",
              showCancelButton: false,
              confirmButtonColor: "#B9DF90",
              confirmButtonText: "Aceptar",
              closeOnConfirm: true
            }).then(function () {
              
                    acceptClick();
                
            });
        break;
        default:

        break;
    }

  }
  launchConfirm(txt, type, acceptClick, cancelClick): void {
  	if(acceptClick == '' || acceptClick == null) {

        acceptClick = function() {

            console.log('nada que hacer');
        }
    }
    if(cancelClick == '' || cancelClick == null) {

        cancelClick = function() {

            console.log('nada que hacer');
        }
    }
    switch (type) {

        //success
        case 1:
           swal({
              title: "Muy bien!",
              text: txt,
              type: "success",
              showCancelButton: true,
              confirmButtonColor: "#B9DF90",
              confirmButtonText: "Aceptar",
              cancelButtonText: "Cancelar",
              cancelButtonColor: "#DA4453",
              closeOnConfirm: true,
              closeOnCancel: true
            }).then(function (isConfirm) {
              
                if (isConfirm) {
                    acceptClick();
                } else {
                    cancelClick();
                }
              
            });
        break;
        //warning
        case 2:
            swal({
              title: "Atención",
              text: txt,
              type: "warning",
              showCancelButton: true,
              confirmButtonColor: "#B9DF90",
              confirmButtonText: "Aceptar",
              cancelButtonText: "Cancelar",
              cancelButtonColor: "#DA4453",
              closeOnConfirm: true,
              closeOnCancel: true
            }).then(function (isConfirm) {
              
                if (isConfirm) {
                    acceptClick();
                } else {
                    cancelClick();
                }
              
            });
        break;
        //error
        case 3:
            swal({
              title: "Error",
              text: txt,
              type: "error",
              showCancelButton: true,
              confirmButtonColor: "#B9DF90",
              confirmButtonText: "Aceptar",
              cancelButtonText: "Cancelar",
              cancelButtonColor: "#DA4453",
              closeOnConfirm: true,
              closeOnCancel: true
            }).then(function (isConfirm) {
              
                if (isConfirm) {
                    acceptClick();
                } else {
                    cancelClick();
                }
              
            });
        break;
        default:

        break;
    }
  }

}