import {Injectable} from "@angular/core";
import {AngularFireAuth} from "angularfire2/auth/auth";
import {Router} from "@angular/router";
import  {Http,Response} from "@angular/http"
import  {Observable} from "rxjs/Observable";
import  {ILogin} from "../login/ILogin";

@Injectable()
export class LoginService{

    private URL:string ="";
    constructor( private router:Router,private _http:Http){
        
    }
   

     login(email, password) {

       
        this.URL ="http://probedelivery.ever-track.com/WS_Login.asmx/ValidateLogin?user="+email+"&password="+password+"";

        return this._http.get(this.URL)
        .map(res => <ILogin> res.json())
        .catch(this.handleError);
    }
    private handleError(error:Response){
        console.log(error);
        return Observable.throw(error.json().error||'Server error');
    }

    
}