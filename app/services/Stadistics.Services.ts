import  {Injectable} from "@angular/core";
import  {Http,Response} from "@angular/http"
import  {Observable} from "rxjs/Observable";
import  {stadistics} from "../dashboard/dashboardMain/stadistics";


import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
@Injectable()
export class  StadisticsService {
	
	userId:number = 5;
	private URL:string ="http://probedelivery.ever-track.com/WS_Dashboard.asmx/EstadisticDelivery?AdminUserId="+this.userId+"";

	constructor(private _http:Http) {}

	 getStadistics():Observable<stadistics[]>{
	 	return this._http
	 	.get(this.URL)
	 	.map((response:Response) => <stadistics[]> response.json().Stadistics)
	 	.do(data =>console.log(data))
	 	.catch(error => {
	 		console.log(error);
	 		return Observable.throw(error.json())
	 	})
	 }

}

