import {Injectable} from "@angular/core";
import {CanActivate} from "@angular/router";
import {Router} from "@angular/router";
@Injectable()
export  class  MyGuard implements CanActivate{

   constructor(private router:Router) {

  }
    canActivate(){
        if(localStorage.getItem(':_cahanel')===null){
             console.log('no loger')
            this.router.navigate(['/login']);
            return false;
        }else{
            console.log('si loger')
            return true;
        }
        
    }
}