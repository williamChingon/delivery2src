import  {Injectable} from "@angular/core";
import  {Http,Response} from "@angular/http"
import  {Observable} from "rxjs/Observable";
import  {IProyects} from "../dashboard/dashboardMain/IProyects";


import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
@Injectable()
export class  ProyectService {
	
	userId:number = 5;
	private URL:string ="http://probedelivery.ever-track.com/WS_Dashboard.asmx/returnProyectsUser?AdminUserId="+this.userId+"";

	constructor(private _http:Http) {}

	 getProyects():Observable<IProyects[]>{
	 	return this._http
	 	.get(this.URL)
	 	.map((response:Response) => <IProyects[]> response.json().Proyects)
	 	.do(data =>console.log(data))
	 	.catch(error => {
	 		console.log(error);
	 		return Observable.throw(error.json())
	 	})
	 }


}

