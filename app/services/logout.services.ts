import  {Injectable} from "@angular/core";
import  {Http,Response} from "@angular/http"
import  {Observable} from "rxjs/Observable";
import  {stadistics} from "../dashboard/dashboardMain/stadistics";
import {Router} from "@angular/router";

import 'rxjs/add/operator/map';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
@Injectable()
export class  LogoutService {

	constructor(private router:Router) {}
	
	logout(){
		localStorage.removeItem(':_cahanel');
		this.router.navigate(['/login']);
	}

}