import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OwlDateTimeIntl, OWL_DATE_TIME_FORMATS} from 'ng-pick-datetime';
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { DynamicTable } from './dynamicTable.component.ts';
import { SearchPipe } from '../pipes/search-pipe';
import { SafePipe } from '../pipes/safe.pipe.ts';

export const DefaultIntl = {
    /** A label for the up second button (used by screen readers).  */
    upSecondLabel: 'Sumar un segundo',

    /** A label for the down second button (used by screen readers).  */
    downSecondLabel: 'Restar un minuto',

    /** A label for the up minute button (used by screen readers).  */
    upMinuteLabel: 'Sumar un minuto',

    /** A label for the down minute button (used by screen readers).  */
    downMinuteLabel: 'Restar un minuto',

    /** A label for the up hour button (used by screen readers).  */
    upHourLabel: 'Sumar una hora',

    /** A label for the down hour button (used by screen readers).  */
    downHourLabel: 'Restar una hora',

    /** A label for the previous month button (used by screen readers). */
    prevMonthLabel: 'Mes anterior',

    /** A label for the next month button (used by screen readers). */
    nextMonthLabel: 'Mes siguiente',

    /** A label for the previous year button (used by screen readers). */
    prevYearLabel: 'Año anterior',

    /** A label for the next year button (used by screen readers). */
    nextYearLabel: 'Año siguiente',

    /** A label for the 'switch to month view' button (used by screen readers). */
    switchToMonthViewLabel: 'Cambiar a vista de mes',

    /** A label for the 'switch to year view' button (used by screen readers). */
    switchToYearViewLabel: 'Cambiar a vista de año',

    /** A label for the cancel button */
    cancelBtnLabel: 'Cerrar',

    /** A label for the set button */
    setBtnLabel: 'Confirmar',

    /** A label for the range 'from' in picker info */
    rangeFromLabel: 'Desde',

    /** A label for the range 'to' in picker info */
    rangeToLabel: 'Hasta',
};
@NgModule({

	providers: [
		{provide: OwlDateTimeIntl, useValue: DefaultIntl},
		{provide: OWL_DATE_TIME_LOCALE, useValue: 'es'}
	],
	imports: [
		CommonModule,
	    OwlDateTimeModule, 
	    OwlNativeDateTimeModule
	 ],
  	declarations: [
    	DynamicTable,
    	SafePipe
    
  	],
  	exports: [
  		DynamicTable,
  	
  	]
})
export class DynamicTableModule { }