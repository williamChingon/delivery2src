import { Component, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { SweetAlertService } from '../services/sweetAlert.service';


declare let jQuery: any;

@Component({
  selector: 'clientes',
  templateUrl: './clientes.template.html'
})
export class Clientes implements  AfterViewInit{
	
	constructor (private sweetAlertService:SweetAlertService){}
	@ViewChild('modalCreate') public modalCreate: ModalDirective;
	@ViewChild('modalEdit') public modalEdit: ModalDirective;

	
	ngAfterViewInit() {
	    jQuery('.createModal').find('.caret').append('<i class="mdi mdi-chevron-down"></i>');
	}
	createClient() {

		this.modalCreate.hide();
		this.launchAdvice('Creando',1,'');
	}
	editClient() {

		this.modalEdit.hide();
		this.launchAdvice('Editando',3,'');
	}
	deleteClient() {

		this.modalEdit.hide();
		this.launchConfirm('¿Desea usted continuar?',2,'',this.launchAdvice('hola',3,''));
	}
	launchAdvice(txt,type,accept) {

		 this.sweetAlertService.launchAdvice(txt, type,accept);
	}
	launchConfirm(txt,type,accept,cancel) {

		 this.sweetAlertService.launchConfirm(txt, type,accept,cancel);
	}
}
