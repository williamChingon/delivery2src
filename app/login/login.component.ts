import { Component, ViewEncapsulation } from '@angular/core';
import { SweetAlertService } from '../services/sweetAlert.service';
import {Router} from "@angular/router";
import {LoginService} from "../services/login.services";
import  {ILogin} from "../login/ILogin";
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'login',
  styleUrls: [ './login.style.scss' ],
  templateUrl: './login.template.html',
  encapsulation: ViewEncapsulation.None,
  host: {
    class: 'login-page app'
  }
})


export class Login {
  constructor(private sweetAlertService:SweetAlertService, private router:Router,private loginservices:LoginService,private _http:HttpClient) {

  }
  Recover = false;
  Login = true;
  private URL:string ="";
  login:any = {};
  data: ILogin[];
   /* data: any={};*/

	ngOnInit():void {

		//alert('Recover:'+ this.Recover +',Login:'+ this.Login);
	}
	ForgotPass():void {

		//alert('hola');
  		if (this.Recover) {

            
            this.Recover = false;
            this.Login = true;
        } else {
            this.Recover = true;
            this.Login = false;
        }

        //alert('Recover:'+ this.Recover +',Login:'+ this.Login);
	}
	 LoginSession():void {

     if(this.login.LogUser==undefined)
     {
          this.sweetAlertService.launchAdvice('Campo usuario vacio', 2, '');
          return;

     }
      if(this.login.LogPass==undefined)
     {
          this.sweetAlertService.launchAdvice('Campo contraseña vacio', 2, '');
          return;

     }

    this.loginservices.login(this.login.LogUser,this.login.LogPass).subscribe(users=> {
     this.data = users["Access"];
      
 
      this.data  = Object.keys(this.data).map((key) => this.data [key]);

      console.log(this.data);
      var logger: Array<ILogin> = JSON.parse(JSON.stringify(this.data));
      let access = logger[0].Success;
      let token =logger[0].Data;
      if(access===true)
      {

        localStorage.setItem(':_cahanel',JSON.stringify(this.data));
        this.router.navigate(['/app/dashboard/dashboardMain']);
      }else
      {

        this.sweetAlertService.launchAdvice('Usuario o Contraseña Incorrectos', 2, '');
      }
   });


	}
	RequestForgot():void {


	}
	fb():void {



	}
	tw():void {


		
	}

}
