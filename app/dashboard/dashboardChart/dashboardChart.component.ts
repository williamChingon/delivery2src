import { Component, ViewEncapsulation } from '@angular/core';

declare let jQuery: any;


@Component({
  selector: 'dashboardChart',
  templateUrl: './dashboardChart.template.html',
  styleUrls: ['../dashboard.style.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardChart {


	nombreEmpresa = 'Empresa Patito';
	
	paqInfo:any = {

		entConcluidas:20,
		entXconcluir:1000,
		rechazadas:2,
		rutConcluidas:1000
	}
	
	ngOnInit(): void {
    

    
		//Pie

	    jQuery('.easy-pie-chart').easyPieChart({
	      barColor: '#ff8d42',
	      trackColor: '#ddd',
	      scaleColor: false,
	      lineWidth: 10,
	      size: 120
	    });
	    jQuery('.easy-pie-chart').data('easyPieChart').update(this.paqInfo.entConcluidas);


	   

    

    
	}
	
}
