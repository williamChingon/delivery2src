import { Component, ViewEncapsulation } from '@angular/core';

declare let jQuery: any;
const TESTPROJECTS = [
  {
    'idProject': '1',
    'name': 'Algernon',    
    'state': 'Palo Alto',
    'dateCompro': 'June 27, 2013',
    'sucursales': 'tal,tal,tal...',
    'status': {
      'progress': '29%',
      'type': 'success'
    }
  },
  {
    'idProject': '2',
    'name': 'Algernon',    
    'state': 'Palo Alto',
    'dateCompro': 'June 27, 2013',
    'sucursales': 'tal,tal,tal...',
    'status': {
      'progress': '29%',
      'type': 'danger'
    }
  }
];
const TESTASK = [
  {
    'idTask': '1',
    'nameTask': 'Algernon',    
    'destinitTask': 'Palo Alto',
    'telTask': 'Palo Alto',
    'mailTask': 'Palo Alto',
    'addressTask': 'Palo Alto',    
    'descTask': 'June 27, 2013',
    'dateComproTask': 'tal,tal,tal...',
    'evidenceLinkTask': 'http://www.google.com'
  },
  {
    'idTask': '1',
    'nameTask': 'Algernon',    
    'destinitTask': 'Palo Alto',
    'telTask': 'Palo Alto',
    'mailTask': 'Palo Alto',
    'addressTask': 'Palo Alto',    
    'descTask': 'June 27, 2013',
    'dateComproTask': 'tal,tal,tal...',
    'evidenceLinkTask': 'http://www.4chan.org'
  },
  {
    'idTask': '1',
    'nameTask': 'Algernon',    
    'destinitTask': 'Palo Alto',
    'telTask': 'Palo Alto',
    'mailTask': 'Palo Alto',
    'addressTask': 'Palo Alto',    
    'descTask': 'June 27, 2013',
    'dateComproTask': 'tal,tal,tal...',
    'evidenceLinkTask': 'http://www.wikipedia.org'
  }
];

@Component({
  selector: 'dashboardMain',
  templateUrl: './dashboardMain.template.html',
  styleUrls: ['../dashboard.style.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DashboardMain {

	data1: any[] = TESTPROJECTS;
  data2: any[] = TESTASK;
	nombreEmpresa = 'Empresa Patito';
	paqInfo:any = {};
	/*paqInfo:any = {

		entConcluidas:20,
		entXconcluir:1000,
		rechazadas:2,
		rutConcluidas:1000
	}*/
	
	ngOnInit(): void {
    

    var o1 = { entConcluidas: 95 };
    var o2 = { entXconcluir: 100 };
    var o3 = { rechazadas: 2 };
    var o4 = { rutConcluidas: 1000 };

    var obj = Object.assign(o1, o2, o3,o4);

    this.paqInfo = obj;
    console.log(obj); // { a: 1, b: 2, c: 3 }
		//Pie

	   
	    //Datatable search engine

	    let searchInput = jQuery('#table-search-input, #search-countries');
	    searchInput
	      .focus((e) => {
	      jQuery(e.target).closest('.input-group').addClass('focus');
	    })
	      .focusout((e) => {
	      jQuery(e.target).closest('.input-group').removeClass('focus');
	    });
	    //this.onChangeTable(this.config);

    

    
	}
	
}
