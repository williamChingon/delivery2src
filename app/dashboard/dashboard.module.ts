
import { NgModule }      from '@angular/core';
import { CommonModule }  from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AlertModule, TooltipModule } from 'ngx-bootstrap';
import { ButtonsModule, BsDropdownModule } from 'ngx-bootstrap';
import { TabsModule, AccordionModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';

import { AgmCoreModule } from 'angular2-google-maps/core';

import 'easy-pie-chart/dist/jquery.easypiechart.js';
import { DataTableModule } from 'angular2-datatable';
import { SearchPipe } from './pipes/search-pipe';
import { SortPipe } from './pipes/sort-pipe';


import { DashboardMain } from './dashboardMain/dashboardMain.component.ts';
import { DashboardMap } from './dashboardMap/dashboardMap.component.ts';
import { DashboardChart } from './dashboardChart/dashboardChart.component.ts';
import { DynamicTableModule } from '../dynamicTable/dynamicTable.module';
import{MyGuard} from '../services/my-guard.service'

import { Widget } from '../layout/widget/widget.directive';


export const routes = [
	{path: '', redirectTo: 'dashboardMain', pathMatch: 'full'},
 	{path: 'dashboardMain', component: DashboardMain, pathMatch: 'full',canActivate:[MyGuard] },
 	{path: 'dashboardMap', component: DashboardMap, pathMatch: 'full' ,canActivate:[MyGuard]},
  {path: 'dashboardChart', component: DashboardChart, pathMatch: 'full' ,canActivate:[MyGuard]}
];


@NgModule({
  imports: [ 
  	CommonModule, 
  	FormsModule, 
  	DataTableModule, 
  	RouterModule.forChild(routes),
  	AlertModule.forRoot(),
    TooltipModule.forRoot(),
    ModalModule,
    ButtonsModule.forRoot(),
    DynamicTableModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDe_oVpi9eRSN99G4o6TwVjJbFBNr58NxE'
    })
  ],
  declarations: [ 
  	DashboardMain, 
  	DashboardMap,
    DashboardChart,    
  	Widget, 
  	SearchPipe,
    SortPipe
    
  ],
  providers:[MyGuard]
})
export class DashboardModule {
  static routes = routes;
}
