import { Component, ViewEncapsulation, ElementRef } from '@angular/core';

declare let jQuery: any;


@Component({
  selector: 'dashboardMap',
  templateUrl: './dashboardMap.template.html',
  styleUrls: ['../dashboard.style.scss'],
  encapsulation: ViewEncapsulation.None
  
})
export class DashboardMap {

	$el: any;
	nombreEmpresa = 'Empresa Patito';
	lat: number = 19.213179;
  lng: number = -99.0;
  zoom: number = 12;
	paqInfo:any = {

		entConcluidas:20,
		entXconcluir:1000,
		rechazadas:2,
		rutConcluidas:1000
	}
  cuantasEntregas = 12;
	listItems:any = [
    
    {idItem: 9, colorcillo: 'black', tipoIcono:1, icono:'radar', mainName:'Pedrito', barrita:'30%', entregas:30, rutas: 50, cuando:'04/03/2016 18:55' },
    {idItem: 9, colorcillo: 'blue', tipoIcono:1, icono:'road-variant', mainName:'Servandito', barrita:'30%', entregas:30, rutas: 50, cuando:'04/03/2016 18:55' },
    {idItem: 9, colorcillo: 'orange', tipoIcono:2, icono:'PD', mainName:'Armandito', barrita:'30%', entregas:30, rutas: 50, cuando:'04/03/2016 18:55' },
    {idItem: 9, colorcillo: 'pink', tipoIcono:1, icono:'oil', mainName:'Robertito', barrita:'30%', entregas:30, rutas: 50, cuando:'04/03/2016 18:55' },
    {idItem: 9, colorcillo: 'purple', tipoIcono:1, icono:'engine-outline', mainName:'Ernestito', barrita:'30%', entregas:30, rutas: 50, cuando:'04/03/2016 18:55' },
    {idItem: 7, colorcillo: 'green', tipoIcono:2, icono:'TS', mainName:'Danielito', barrita:'80%', entregas:2, rutas: 25, cuando:'Ayer' }
    
  ];
  dashItems:any = [
    
    {idItem: 9, dashIcon:'speedometer', extraIcon:'', lectura:'255500', units:'km/h'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#1', lectura:'275', units:'K'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#2', lectura:'2', units:'K'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#3', lectura:'2', units:'K'},
    {idItem: 9, dashIcon:'gas-station', extraIcon:'', lectura:'95', units:'%'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#1', lectura:'275', units:'K'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#2', lectura:'2', units:'K'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#3', lectura:'2', units:'K'},
    {idItem: 9, dashIcon:'gas-station', extraIcon:'', lectura:'95', units:'%'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#1', lectura:'275', units:'K'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#2', lectura:'2', units:'K'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#3', lectura:'2', units:'K'},
    {idItem: 9, dashIcon:'gas-station', extraIcon:'', lectura:'95', units:'%'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#1', lectura:'275', units:'K'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#2', lectura:'2', units:'K'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#3', lectura:'2', units:'K'},
    {idItem: 9, dashIcon:'gas-station', extraIcon:'', lectura:'95', units:'%'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#1', lectura:'275', units:'K'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#2', lectura:'2', units:'K'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#3', lectura:'2', units:'K'},
    {idItem: 9, dashIcon:'gas-station', extraIcon:'', lectura:'95', units:'%'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#1', lectura:'275', units:'K'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#2', lectura:'2', units:'K'},
    {idItem: 9, dashIcon:'thermometer', extraIcon:'#3', lectura:'2', units:'K'},
    {idItem: 9, dashIcon:'gas-station', extraIcon:'', lectura:'95', units:'%'},
    {idItem: 9, dashIcon:'car-battery', extraIcon:'', lectura:'12', units:'V'}
    
    
    
  ];
  dispatchItems:any = [
    
    {idItem: 9, colorcillo: 'black', icono:'radar', mainName:'En ruta', cuando:'04/03/2016 18:55' },
    {idItem: 9, colorcillo: 'blue', icono:'road-variant', mainName:'En ruta', cuando:'04/03/2016 18:55' },
    {idItem: 9, colorcillo: 'orange', icono:'road-variant', mainName:'En ruta', cuando:'04/03/2016 18:55' },
    {idItem: 9, colorcillo: 'pink', icono:'oil', mainName:'En ruta', cuando:'04/03/2016 18:55' },
    {idItem: 9, colorcillo: 'purple', icono:'engine-outline', mainName:'En ruta', cuando:'04/03/2016 18:55' },
    {idItem: 7, colorcillo: 'green', icono:'road-variant', mainName:'En ruta', cuando:'Ayer' }
    
    
    
  ];
  constructor(el: ElementRef) {
    this.$el = jQuery(el.nativeElement);
    
  }
  initMapListScroll(): void {
    let $sidebarContent = this.$el.find('#mapEntryList');
    if (this.$el.find('.slimScrollDiv').length !== 0) {
      $sidebarContent.slimscroll({
        destroy: true
      });
    }
    $sidebarContent.slimscroll({
      height: '300px',
      size: '4px'
    });
  }
	ngOnInit(): void {
    
    jQuery(window).on('sn:resize', this.initMapListScroll.bind(this));
    this.initMapListScroll();
    let $newActiveLink =  jQuery('a[href="#/app/dashboard/dashboardMain"]');
    $newActiveLink.closest('li').addClass('active').parents('li').addClass('active');    
    jQuery('.selectpicker').selectpicker();

    //Datatable search engine

      let searchInput = jQuery('#transparent-input');
      searchInput
        .focus((e) => {
        jQuery(e.target).closest('.input-group').addClass('focus');
      })
        .focusout((e) => {
        jQuery(e.target).closest('.input-group').removeClass('focus');
      });

      
	}
	
}
