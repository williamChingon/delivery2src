import { Component, OnInit, ElementRef } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Location } from '@angular/common';
import { AppConfig } from '../../app.config';
declare let jQuery: any;

@Component({
  selector: '[sidebar]',
  templateUrl: './sidebar.template.html'
})

export class Sidebar implements OnInit {
  $el: any;
  config: any;
  router: Router;
  location: Location;
  secciones:any = [
    {id: 0, nombre: 'Dashboard', icono:'mdi mdi-chart-bar', activo:true, ruta: 'dashboard/dashboardMain'},
    {id: 1, nombre: 'Clientes',icono:'mdi mdi-account-multiple', activo:true, ruta: 'clientes'},
    {id: 2, nombre: 'Proyectos',icono:'mdi mdi-account-settings-variant', activo:true, ruta: 'proyectos'},
    {id: 3, nombre: 'Ruta',icono:'mdi mdi-road-variant', activo:true, ruta: 'ruta'},
    {id: 4, nombre: 'Usuarios',icono:'mdi mdi-account', activo:true, ruta: 'usuarios'},
    {id: 5, nombre: 'Importador',icono:'mdi mdi-upload', activo:true, ruta: 'importador'},
    {id: 6, nombre: 'Recolección',icono:'mdi mdi-cube-outline', activo:true, ruta: 'recoleccion'},
    {id: 7, nombre: 'Asignación de equipos',icono:'mdi mdi-cellphone-settings', activo:true, ruta: 'asignacion'},
    {id: 8, nombre: 'No recuerdo',icono:'mdi mdi-map-marker-plus', activo:true, ruta: 'no-recuerdo'},
    {id: 9, nombre: 'Perfiles',icono:'mdi mdi-clipboard-account', activo:true, ruta: 'perfiles'},
    {id: 10, nombre: 'Sucursales',icono:'mdi mdi-store', activo:true, ruta: 'sucursales'}
    
  ]; 

  constructor(config: AppConfig, el: ElementRef, router: Router, location: Location) {
    this.$el = jQuery(el.nativeElement);
    this.config = config.getConfig();
    this.router = router;
    this.location = location;
  }

  initSidebarScroll(): void {
    let $sidebarContent = this.$el.find('.js-sidebar-content');
    if (this.$el.find('.slimScrollDiv').length !== 0) {
      $sidebarContent.slimscroll({
        destroy: true
      });
    }
    $sidebarContent.slimscroll({
      height: window.innerHeight,
      size: '4px'
    });
  }

  changeActiveNavigationItem(location): void {
    let $newActiveLink = this.$el.find('a[href="#' + location.path() + '"]');

    // collapse .collapse only if new and old active links belong to different .collapse
    if (!$newActiveLink.is('.active > .collapse > li > a')) {
      this.$el.find('.active .active').closest('.collapse').collapse('hide');
    }
    this.$el.find('.sidebar-nav .active').removeClass('active');

    $newActiveLink.closest('li').addClass('active')
      .parents('li').addClass('active');

    // uncollapse parent
    $newActiveLink.closest('.collapse').addClass('in')
      .siblings('a[data-toggle=collapse]').removeClass('collapsed');
  }

  ngAfterViewInit(): void {
    this.changeActiveNavigationItem(this.location);
  }

  toggleSidebarOverflow($event) {
    jQuery('#sidebar').css('z-index', $event ? '2' : '0' );
    jQuery('.js-sidebar-content, .slimScrollDiv').css('overflow', $event ? 'visible' : 'hidden');
  }

  ngOnInit(): void {
    jQuery(window).on('sn:resize', this.initSidebarScroll.bind(this));
    this.initSidebarScroll();

    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.changeActiveNavigationItem(this.location);
      }
    });
  }
}
