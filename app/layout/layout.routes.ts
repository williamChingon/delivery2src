import { Routes, RouterModule }  from '@angular/router';
import { Layout } from './layout.component';
// noinspection TypeScriptValidateTypes
const routes: Routes = [
  { path: '', component: Layout, children: [
    { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
    { path: 'dashboard', loadChildren: '../dashboard/dashboard.module#DashboardModule' },
    { path: 'clientes', loadChildren: '../clientes/clientes.module#ClientesModule' },
    { path: 'sucursales', loadChildren: '../sucursales/sucursales.module#SucursalesModule' },
    { path: 'proyectos', loadChildren: '../proyectos/proyectos.module#ProyectosModule' },
    { path: 'ruta', loadChildren: '../ruta/ruta.module#RutaModule' },
    { path: 'usuarios', loadChildren: '../usuarios/usuarios.module#UsuariosModule' }
  ]}
];

export const ROUTES = RouterModule.forChild(routes);
