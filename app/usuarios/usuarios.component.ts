import { Component, AfterViewInit, OnInit, ViewChild } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { SweetAlertService } from '../services/sweetAlert.service';


declare let jQuery: any;

@Component({
  selector: 'usuarios',
  templateUrl: './usuarios.template.html'
})
export class Usuarios implements  AfterViewInit{
	
	constructor (private sweetAlertService:SweetAlertService){}
	@ViewChild('modalCreate') public modalCreate: ModalDirective;
	@ViewChild('modalEdit') public modalEdit: ModalDirective;

	
	ngAfterViewInit() {
	    jQuery('.createModal').find('.caret').append('<i class="mdi mdi-chevron-down"></i>');
	}
	createUser() {

		this.modalCreate.hide();
		this.launchAdvice('Creando',1,'');
	}
	editUser() {

		this.modalEdit.hide();
		this.launchAdvice('Editando',3,'');
	}
	deleteUser() {

		this.modalEdit.hide();
		this.launchConfirm('¿Desea usted continuar?',2,'',this.launchAdvice('hola',3,''));
	}
	launchAdvice(txt,type,accept) {

		 this.sweetAlertService.launchAdvice(txt, type,accept);
	}
	launchConfirm(txt,type,accept,cancel) {

		 this.sweetAlertService.launchConfirm(txt, type,accept,cancel);
	}
}
